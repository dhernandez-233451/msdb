-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-06-2019 a las 23:12:05
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.1.30-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mssystem`
--
CREATE DATABASE IF NOT EXISTS `mssystem` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mssystem`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections`
--

DROP TABLE IF EXISTS `connections`;
CREATE TABLE IF NOT EXISTS `connections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `host` varchar(16) NOT NULL,
  `sysnr` varchar(5) NOT NULL,
  `client` varchar(3) NOT NULL,
  `user` varchar(12) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `saprouter` varchar(26) DEFAULT NULL,
  `function` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `connections`
--

INSERT INTO `connections` (`id`, `name`, `host`, `sysnr`, `client`, `user`, `passwd`, `lang`, `saprouter`, `function`) VALUES
(1, 'MiniSAP', '192.168.253.12', '00', '001', 'bcuser', '', 'EN', '', 'ZOTRORFC'),
(2, 'GMX', '192.168.0.107', '00', '100', 'abap01', '', 'ES', '187.216.75.82', 'ZFMMD_RFC_P');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `modules`
--

INSERT INTO `modules` (`id`, `name`) VALUES
(1, '(Usuarios) Perfiles'),
(2, '(Usuarios) Usuarios'),
(4, '(BD) Conexión SAP'),
(5, '(BD) Administración local'),
(6, '(Datos SAP) Tablas SAP'),
(7, '(Datos SAP) Campos SAP'),
(8, '(LSMW) Layouts LSMW'),
(9, '(LSMW) Campos LSMW'),
(10, '(Mapeo) Mapeo'),
(11, '(Mapeo) Reglas'),
(12, '(Depuración) Descarga layout'),
(13, '(Depuración) Carga layout');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permits`
--

DROP TABLE IF EXISTS `permits`;
CREATE TABLE IF NOT EXISTS `permits` (
  `id_profile` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  KEY `id_profile` (`id_profile`),
  KEY `id_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permits`
--

INSERT INTO `permits` (`id_profile`, `id_module`) VALUES
(1, 1),
(1, 2),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(2, 1),
(2, 2),
(2, 4),
(2, 5),
(2, 6),
(2, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profiles`
--

INSERT INTO `profiles` (`id`, `name`) VALUES
(1, 'administrador'),
(2, 'Prueba'),
(4, 'root'),
(6, 'Perfil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rules`
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rules`
--

INSERT INTO `rules` (`id`, `name`) VALUES
(1, 'MAYÚSCULAS'),
(2, 'minúsculas'),
(3, 'Modo oración.'),
(4, 'Mayúscula Al Principio De Cada Palabra'),
(5, 'Valor obligatorio'),
(6, 'Únicamente letras'),
(7, 'Únicamente números'),
(8, 'Sin acentos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `user` varchar(30) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `session` varchar(30) DEFAULT NULL,
  `access_attempt` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_profile` (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `id_profile`, `name`, `user`, `passwd`, `session`, `access_attempt`) VALUES
(1, 1, 'Daniel Hernández', 'dan@wwn.com.mx', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 0),
(2, 2, 'Liliana García', 'lili@wwn.com.mx', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 0);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `permits`
--
ALTER TABLE `permits`
  ADD CONSTRAINT `permits_ibfk_1` FOREIGN KEY (`id_profile`) REFERENCES `profiles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `permits_ibfk_2` FOREIGN KEY (`id_module`) REFERENCES `modules` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_profile`) REFERENCES `profiles` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
