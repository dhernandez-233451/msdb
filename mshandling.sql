-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-06-2019 a las 23:11:19
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.1.30-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mshandling`
--
CREATE DATABASE IF NOT EXISTS `mshandling` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mshandling`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lsmws`
--

DROP TABLE IF EXISTS `lsmws`;
CREATE TABLE IF NOT EXISTS `lsmws` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lsmws`
--

INSERT INTO `lsmws` (`id`, `title`) VALUES
(1, 'KE51'),
(3, 'KS01'),
(4, 'SPFLI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lsmw_data`
--

DROP TABLE IF EXISTS `lsmw_data`;
CREATE TABLE IF NOT EXISTS `lsmw_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lsmw_fields` int(11) NOT NULL,
  `value` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_lsmw_layout` (`id_lsmw_fields`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lsmw_data`
--

INSERT INTO `lsmw_data` (`id`, `id_lsmw_fields`, `value`) VALUES
(19, 6, 'DANIEL Hernández'),
(20, 6, 'AA'),
(21, 6, 'AZ'),
(22, 6, 'AZ'),
(23, 6, 'AZ'),
(24, 6, 'AZ'),
(25, 6, 'DL'),
(26, 6, 'DL'),
(27, 6, 'DL'),
(28, 6, 'JL'),
(29, 6, 'JL'),
(30, 6, 'LH'),
(31, 6, 'LH'),
(32, 6, 'LH'),
(33, 6, 'LH'),
(34, 6, 'LH'),
(35, 6, 'QF'),
(36, 6, 'QF'),
(37, 6, 'SQ'),
(38, 6, 'SQ'),
(39, 6, 'SQ'),
(40, 6, 'SQ'),
(41, 6, 'UA'),
(42, 6, 'UA'),
(43, 6, 'UA'),
(44, 6, 'UA'),
(45, 7, '0017'),
(46, 7, '0064'),
(47, 7, '0555'),
(48, 7, '0788'),
(49, 7, '0789'),
(50, 7, '0790'),
(51, 7, '0106'),
(52, 7, '1699'),
(53, 7, '1984'),
(54, 7, '0407'),
(55, 7, '0408'),
(56, 7, '0400'),
(57, 7, '0401'),
(58, 7, '0402'),
(59, 7, '2402'),
(60, 7, '2407'),
(61, 7, '0005'),
(62, 7, '0006'),
(63, 7, '0002'),
(64, 7, '0015'),
(65, 7, '0158'),
(66, 7, '0988'),
(67, 7, '0941'),
(68, 7, '3504'),
(69, 7, '3516'),
(70, 7, '3517'),
(71, 8, 'US'),
(72, 8, 'US'),
(73, 8, 'IT'),
(74, 8, 'IT'),
(75, 8, 'JP'),
(76, 8, 'IT'),
(77, 8, 'US'),
(78, 8, 'US'),
(79, 8, 'US'),
(80, 8, 'JP'),
(81, 8, 'DE'),
(82, 8, 'DE'),
(83, 8, 'US'),
(84, 8, 'DE'),
(85, 8, 'DE'),
(86, 8, 'DE'),
(87, 8, 'SG'),
(88, 8, 'DE'),
(89, 8, 'SG'),
(90, 8, 'US'),
(91, 8, 'SG'),
(92, 8, 'SG'),
(93, 8, 'DE'),
(94, 8, 'US'),
(95, 8, 'US'),
(96, 8, 'DE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lsmw_fields`
--

DROP TABLE IF EXISTS `lsmw_fields`;
CREATE TABLE IF NOT EXISTS `lsmw_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lsmw` int(11) NOT NULL,
  `field` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_lsmw` (`id_lsmw`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lsmw_fields`
--

INSERT INTO `lsmw_fields` (`id`, `id_lsmw`, `field`) VALUES
(1, 1, 'PRCTR'),
(3, 3, 'KOKRS'),
(4, 3, 'KOSTL'),
(5, 3, 'DATAB_ANFO'),
(6, 4, 'CARRID'),
(7, 4, 'CONNID'),
(8, 4, 'COUNTRYFR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mapping`
--

DROP TABLE IF EXISTS `mapping`;
CREATE TABLE IF NOT EXISTS `mapping` (
  `id_lsmw_field` int(11) NOT NULL,
  `id_sap_field` int(11) NOT NULL,
  KEY `id_lsmw_layout` (`id_lsmw_field`),
  KEY `id_sap_field` (`id_sap_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mapping`
--

INSERT INTO `mapping` (`id_lsmw_field`, `id_sap_field`) VALUES
(4, 8),
(3, 1),
(6, 9),
(7, 10),
(8, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_data`
--

DROP TABLE IF EXISTS `pre_data`;
CREATE TABLE IF NOT EXISTS `pre_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lsmw_layout` int(11) NOT NULL,
  `session` varchar(30) DEFAULT NULL,
  `value` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rules_lsmw`
--

DROP TABLE IF EXISTS `rules_lsmw`;
CREATE TABLE IF NOT EXISTS `rules_lsmw` (
  `id_rule` int(11) NOT NULL,
  `id_lsmw_field` int(11) NOT NULL,
  KEY `id_ruler` (`id_rule`),
  KEY `id_lsmw_layout` (`id_lsmw_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rules_lsmw`
--

INSERT INTO `rules_lsmw` (`id_rule`, `id_lsmw_field`) VALUES
(2, 1),
(4, 1),
(1, 3),
(2, 3),
(8, 3);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `lsmw_data`
--
ALTER TABLE `lsmw_data`
  ADD CONSTRAINT `lsmw_data_ibfk_1` FOREIGN KEY (`id_lsmw_fields`) REFERENCES `lsmw_fields` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `lsmw_fields`
--
ALTER TABLE `lsmw_fields`
  ADD CONSTRAINT `lsmw_fields_ibfk_1` FOREIGN KEY (`id_lsmw`) REFERENCES `lsmws` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `mapping`
--
ALTER TABLE `mapping`
  ADD CONSTRAINT `mapping_ibfk_1` FOREIGN KEY (`id_lsmw_field`) REFERENCES `lsmw_fields` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mapping_ibfk_2` FOREIGN KEY (`id_sap_field`) REFERENCES `msmirror`.`sap_fields` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `rules_lsmw`
--
ALTER TABLE `rules_lsmw`
  ADD CONSTRAINT `rules_lsmw_ibfk_1` FOREIGN KEY (`id_rule`) REFERENCES `mssystem`.`rules` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `rules_lsmw_ibfk_2` FOREIGN KEY (`id_lsmw_field`) REFERENCES `lsmw_fields` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
