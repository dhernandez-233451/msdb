-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-06-2019 a las 23:11:43
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.1.30-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `msmirror`
--
CREATE DATABASE IF NOT EXISTS `msmirror` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `msmirror`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sap_data`
--

DROP TABLE IF EXISTS `sap_data`;
CREATE TABLE IF NOT EXISTS `sap_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sap_field` int(11) NOT NULL,
  `value` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_sap_field` (`id_sap_field`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sap_data`
--

INSERT INTO `sap_data` (`id`, `id_sap_field`, `value`) VALUES
(1, 9, 'AA'),
(2, 9, 'AA'),
(3, 9, 'AZ'),
(4, 9, 'AZ'),
(5, 9, 'AZ'),
(6, 9, 'AZ'),
(7, 9, 'DL'),
(8, 9, 'DL'),
(9, 9, 'DL'),
(10, 9, 'JL'),
(11, 9, 'JL'),
(12, 9, 'LH'),
(13, 9, 'LH'),
(14, 9, 'LH'),
(15, 9, 'LH'),
(16, 9, 'LH'),
(17, 9, 'QF'),
(18, 9, 'QF'),
(19, 9, 'SQ'),
(20, 9, 'SQ'),
(21, 9, 'SQ'),
(22, 9, 'SQ'),
(23, 9, 'UA'),
(24, 9, 'UA'),
(25, 9, 'UA'),
(26, 9, 'UA'),
(27, 10, '0017'),
(28, 10, '0064'),
(29, 10, '0555'),
(30, 10, '0788'),
(31, 10, '0789'),
(32, 10, '0790'),
(33, 10, '0106'),
(34, 10, '1699'),
(35, 10, '1984'),
(36, 10, '0407'),
(37, 10, '0408'),
(38, 10, '0400'),
(39, 10, '0401'),
(40, 10, '0402'),
(41, 10, '2402'),
(42, 10, '2407'),
(43, 10, '0005'),
(44, 10, '0006'),
(45, 10, '0002'),
(46, 10, '0015'),
(47, 10, '0158'),
(48, 10, '0988'),
(49, 10, '0941'),
(50, 10, '3504'),
(51, 10, '3516'),
(52, 10, '3517'),
(53, 11, 'US'),
(54, 11, 'US'),
(55, 11, 'IT'),
(56, 11, 'IT'),
(57, 11, 'JP'),
(58, 11, 'IT'),
(59, 11, 'US'),
(60, 11, 'US'),
(61, 11, 'US'),
(62, 11, 'JP'),
(63, 11, 'DE'),
(64, 11, 'DE'),
(65, 11, 'US'),
(66, 11, 'DE'),
(67, 11, 'DE'),
(68, 11, 'DE'),
(69, 11, 'SG'),
(70, 11, 'DE'),
(71, 11, 'SG'),
(72, 11, 'US'),
(73, 11, 'SG'),
(74, 11, 'SG'),
(75, 11, 'DE'),
(76, 11, 'US'),
(77, 11, 'US'),
(78, 11, 'DE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sap_fields`
--

DROP TABLE IF EXISTS `sap_fields`;
CREATE TABLE IF NOT EXISTS `sap_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sap_table` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_sap_table` (`id_sap_table`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sap_fields`
--

INSERT INTO `sap_fields` (`id`, `id_sap_table`, `name`) VALUES
(1, 2, 'VBELN'),
(2, 2, 'ERDAT'),
(4, 2, 'ERZET'),
(7, 3, 'VBELN'),
(8, 3, 'POSNR'),
(9, 4, 'CARRID'),
(10, 4, 'CONNID'),
(11, 4, 'COUNTRYFR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sap_tables`
--

DROP TABLE IF EXISTS `sap_tables`;
CREATE TABLE IF NOT EXISTS `sap_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sap_tables`
--

INSERT INTO `sap_tables` (`id`, `name`) VALUES
(2, 'VBAK'),
(3, 'VBAP'),
(4, 'SPFLI');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sap_data`
--
ALTER TABLE `sap_data`
  ADD CONSTRAINT `sap_data_ibfk_1` FOREIGN KEY (`id_sap_field`) REFERENCES `sap_fields` (`id`);

--
-- Filtros para la tabla `sap_fields`
--
ALTER TABLE `sap_fields`
  ADD CONSTRAINT `sap_fields_ibfk_1` FOREIGN KEY (`id_sap_table`) REFERENCES `sap_tables` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
